# Emacs Template

A minimal emacs configuration for haskell programming using [haskell-mode](http://haskell.github.io/haskell-mode) and friends.

Package management approach is based on [this tutorial](http://y.tsutsumi.io/emacs-from-scratch-part-2-package-management.html).

To install this configuration, clone the repository and place the following files at your home directory:

- `.emacs` - emacs will call this file first which will trigger the whole thing. Also contains a few convenient tweaks.
- `.emacs.d/my-packages.el` - contains the list of packages to install when starting emacs
- `.emacs.d/my-loadpackages.el` - defines how to enable and setup the packages we use

So the steps are:

```
git clone http://gitlab.com/gilmi/emacs-haskell-mode-starter
cp emacs-haskell-mode-starter/.emacs  ~/
cp -r emacs-haskell-mode-starter/.emacs.d ~/
```

Now just run Emacs and start hacking!

