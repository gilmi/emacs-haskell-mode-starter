
(load "~/.emacs.d/my-packages.el")


(global-eldoc-mode -1)


;; Uncomment this for vim bindings
;(require 'evil)
;
;(evil-mode 1)
;
;(with-eval-after-load 'evil-maps
; (define-key evil-motion-state-map (kbd "q") nil)
; (define-key evil-motion-state-map (kbd "SPC") nil)
; (define-key evil-visual-state-map (kbd "SPC") nil)
; (define-key evil-motion-state-map (kbd "RET") nil)
; (define-key evil-visual-state-map (kbd "RET") nil)
; (define-key evil-motion-state-map (kbd "TAB") nil)
; (define-key evil-visual-state-map (kbd "TAB") nil)
;
; (define-key evil-normal-state-map (kbd "C-.") nil)
; (define-key evil-normal-state-map (kbd "M-.") nil)
; (define-key evil-motion-state-map "h" nil)
; (define-key evil-motion-state-map "j" nil)
; (define-key evil-motion-state-map "k" nil)
; (define-key evil-motion-state-map "l" nil)

; (define-key evil-motion-state-map "j" 'evil-backward-char)
; (define-key evil-motion-state-map "k" 'evil-next-line)
; (define-key evil-motion-state-map "l" 'evil-previous-line)
; (define-key evil-motion-state-map ";" 'evil-forward-char)

;)


(require 'color-theme)
(set-frame-parameter nil 'background-mode 'dark)
(set-terminal-parameter nil 'background-mode 'dark)

(load-theme 'base16-monokai t)


;; magit

(require 'magit)


;; auto completion

(add-hook 'after-init-hook 'global-company-mode)
(global-set-key (kbd "C-c w") 'company-complete)
(setq company-minimum-prefix-length 1)
(setq company-dabbrev-downcase 0)
(setq company-idle-delay 0.2)

;; flycheck

(require 'flycheck)

(add-hook 'after-init-hook #'global-flycheck-mode)

(require 'flycheck-pos-tip)
(with-eval-after-load 'flycheck
  (flycheck-pos-tip-mode))

(setq flycheck-pos-tip-timeout 30)

(with-eval-after-load 'flycheck
  (setq-default flycheck-disabled-checkers '(emacs-lisp-checkdoc)))

(require 'flycheck-color-mode-line)
(add-hook 'flycheck-mode-hook
  'flycheck-color-mode-line-mode)

(global-set-key [f9] 'flycheck-list-errors)

(setq flycheck-color-mode-line-show-running nil)

;;;;;;;;;;;;;
;;         ;;
;; HASKELL ;;
;;         ;;
;;;;;;;;;;;;;


(require 'haskell)


(require 'haskell-mode)
(add-hook 'haskell-mode-hook 'haskell-indentation-mode)



(require 'haskell-interactive-mode)
(require 'haskell-process)
(add-hook 'haskell-mode-hook 'interactive-haskell-mode)


(require 'flycheck-haskell)
(eval-after-load 'flycheck
  '(add-hook 'flycheck-mode-hook #'flycheck-haskell-setup))


(define-key haskell-mode-map (kbd "C-`") nil)
(define-key haskell-mode-map (kbd "C-c C-`") 'haskell-interactive-bring)
(define-key haskell-mode-map (kbd "C-c C-l") 'haskell-process-load-file)
(define-key haskell-mode-map (kbd "C-l C-l") 'haskell-process-load-file)
(define-key haskell-mode-map (kbd "C-c .") 'haskell-process-do-type)
(define-key haskell-mode-map (kbd "C-c ?") 'haskell-process-do-info)
(define-key haskell-mode-map (kbd "C-c C-k") 'haskell-interactive-mode-clear)
(define-key haskell-mode-map (kbd "C-c C-p") 'haskell-session-change-target)
(define-key haskell-mode-map (kbd "M-.") 'haskell-mode-jump-to-def-or-tag)
(define-key haskell-mode-map (kbd "C-:") 'haskell-search-hoogle)



;; run hasktags on save
(custom-set-variables
  '(haskell-tags-on-save t))
;; if emacs complain, run: M-x tags-reset-tags-tables

(setq haskell-interactive-mode-eval-mode 'haskell-mode)

(defun haskell-interactive-toggle-print-mode ()
  (interactive)
  (setq haskell-interactive-mode-eval-mode (intern
         (ido-completing-read "Eval result mode: "
                              '("fundamental-mode"
                              "haskell-mode"
                              "espresso-mode"
                              "ghc-core-mode"
                              "org-mode")))))

(define-key haskell-interactive-mode-map (kbd "C-c C-v") 'haskell-interactive-toggle-print-mode)

(require 'ghc)

;; (add-hook 'haskell-mode-hook (lambda () (ghc-init)))

(defun my-ghc-haskell-mode-hook ()
   (ghc-abbrev-init)
   (ghc-type-init)
   (unless my-ghc-initialized
     (ghc-comp-init)
     (setq my-ghc-initialized t))
   (ghc-import-module))

;(with-eval-after-load 'company
;   (add-to-list 'company-backends 'company-ghc))

(add-hook 'haskell-mode-hook
          (lambda ()
            (set (make-local-variable 'company-backends)
                 (append '((company-capf company-dabbrev-code))
                         company-backends))))


