;; .emacs

;; default to better frame titles
(setq frame-title-format
      "%b - You can change this title if you look at your ~/.emacs file")

;; default to unified diffs
(setq diff-switches "-u")

;; always end a file with a newline
;(setq require-final-newline 'query)


(add-to-list 'load-path
  "~"
)
(add-to-list 'exec-path
  "/usr/local/bin"
)
(add-to-list 'exec-path
  "~/.local/bin"
)

(setq-default indent-tabs-mode nil)


(defun display-startup-echo-area-message ()
    (message "Let the hacking begin!"))

(setq initial-scratch-message "")

(setq resize-mini-windows t) ; grow and shrink as necessary
(setq max-mini-window-height 10) ; grow up to max of 10 lines

(setq minibuffer-scroll-window t)

;; enable visual feedback on selections
(setq transient-mark-mode t)


;; line numbers
(global-linum-mode 1)


;; scrolling
(setq scroll-step 1
   scroll-conservatively  10000)


;; auto indent
(define-key global-map (kbd "RET") 'newline-and-indent)


;; revert buffer
(define-key global-map (kbd "<f10>") 'revert-buffer)

;; parens
(show-paren-mode 1)

;; Warn before you exit emacs!
(setq confirm-kill-emacs 'yes-or-no-p)

;; make all "yes or no" prompts show "y or n" instead
(fset 'yes-or-no-p 'y-or-n-p)

;; I use version control, don't annoy me with backup files everywhere
(setq make-backup-files nil)
(setq auto-save-default nil)

; search work under the cursor in hoogle

(setq browse-url-generic-program (executable-find "firefox"))

(defun haskell-search-hoogle ()
  "Search hoogle for the word under the cursor"
  (interactive)
  (browse-url-generic (concat "https://hoogle.haskell.org/?hoogle=" (thing-at-point 'word))))

(setq c-default-style "linux" c-basic-offset 4)
(setq-default c-electric-flag nil)
(setq-default c++-electric-flag nil)

(setq compilation-skip-threshold 2)

;;eval buffer
(define-key global-map (kbd "C-c C-b") 'eval-buffer)

(global-set-key (kbd "M-g M-f") 'first-error)
(global-set-key (kbd "M-g M-d") 'next-error)

(global-set-key (kbd "C-c o") 'compile)
(global-set-key (kbd "C-c f") 'find-grep)
(global-set-key (kbd "C-c l") 'locate)
(global-set-key (kbd "C-z") nil)

;; cycle through buffers with Ctrl-Tab
(global-set-key (kbd "<C-tab>") 'other-window)


(electric-indent-mode 0)

;;;;;;;;;;;;;;;;;;
;;;; Packages ;;;;
;;;;;;;;;;;;;;;;;;

;;start loading the packages
(package-initialize)


;; load packages
(load "~/.emacs.d/my-loadpackages.el")

